#Imagen Base
FROM alpine

#Quien lo mantiene
MAINTAINER Ivan Barreda <i.barreda@gmail.com>

#indica que el puerto 3000 se tiene que exponer en la instalacion


#Iniciamos el sistema
#Añadidos un editor humano 
#Creamos la carpeta del server
RUN echo ==== Updating System AND Basic config  ====
RUN apk update
RUN apk upgrade
RUN apk add nano
RUN apk add git
RUN apk add openssh
RUN mkdir /home/server
WORKDIR /home/server/


#Instalamos Node
#Clonamos la base de Express del Github
#Instalamos Express
RUN echo ==== Installing NodeJS ====
RUN apk add nodejs
RUN git clone https://github.com/cuxaro/NodeJS-Docker.git /home/server
RUN cd /home/server && npm install

#Instalamos y Arrancamos el server con PM2 para que trabaje en background
RUN echo ==== Installing PM2 ====
RUN npm install pm2 -g
#RUN echo ==== Installing PM2 ====
#RUN pm2 start /home/server/app.js --name FiestasApp-Express

#Comando Final
EXPOSE 3000
CMD ["/bin/sh"]